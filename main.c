#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>



#ifndef WIN32
    #include <sys/types.h>
#endif

#define TAILLE_MAX 1000 // Tableau de taille 1000

typedef struct Date
{
    int jour;
    int mois;
    int an;
}Date;

typedef struct Heure{

    int h;
    int m;
    int s;
}Heure;

typedef struct Lieu
{
    double longi;
    double lati;

}Lieu;


typedef struct Couple
{
    int ppm;
    Date date;
    Lieu lieu;
    Heure heure;

}Couple;

typedef struct CelluleC
{
    Couple cou;
    struct CelluleC *suivant;
}CelluleC;

typedef struct CelluleM
{
    int nombre;
    struct CelluleM *suivant;
}CelluleM;

typedef struct ListeC
{
    CelluleC *premier;
}ListeC ;

typedef struct ListeM
{
    struct CelluleM *premier;
}ListeM;

typedef  struct PPM {
    int O;
    int P;
    int C;
    int S;
    int N;
}PPM;


ListeC *initialisationC()
{
    ListeC *liste = malloc(sizeof(*liste));
    CelluleC *cellule = malloc(sizeof(*cellule));

    if (liste == NULL || cellule == NULL)
    {
        exit(EXIT_FAILURE);
    }


    cellule->cou.ppm = -1;
    cellule->cou.date.an = 0;
    cellule->cou.date.jour = 0;
    cellule->cou.date.mois = 0;
    cellule->cou.lieu.lati = 0.000;
    cellule->cou.lieu.longi=0.000;
    cellule->cou.heure.h = 00;
    cellule->cou.heure.m = 00;
    cellule->cou.heure.s = 00;
    cellule->suivant = NULL;
    liste->premier = cellule;

    return liste;

}

ListeM *initialisation()
{
    ListeM *liste = malloc( sizeof(*liste));
    CelluleM *cellule = malloc(sizeof(*cellule));

    if (liste == NULL || cellule == NULL)
    {
        exit(EXIT_FAILURE);
    }

    cellule->nombre = -1;
    cellule->suivant = NULL;
    liste->premier = cellule;

    return liste;
}

void insertionC(ListeM *liste, int a)
{
    CelluleM *nouveau = malloc(sizeof(*nouveau));

    if (liste == NULL || nouveau == NULL)
    {

        exit(EXIT_FAILURE);
    }

    nouveau->nombre = a;

    /* Insertion de l'élémentment au début de la liste */
    nouveau->suivant = liste->premier;
    liste->premier = nouveau;

}

void moyenne (ListeM *liste)
{
    if (liste == NULL)
    {
        exit(EXIT_FAILURE);
    }
    printf("Calcule de la moyenne en cours ... \n");
    CelluleM *actuel = liste->premier;

    float a = 0;
    int b = 0;
    int min = 1000;
    int max = 0;
    float moy;
    while (actuel->nombre != -1)
    {
        if(actuel->nombre < min)
            min = actuel->nombre;
        if (actuel->nombre > max)
            max = actuel->nombre;

        a +=  actuel->nombre;
        actuel = actuel->suivant;
        b++;
    }

    moy = a/b;


    printf("Moyenne : %f minimum : %d maximum : %d\n", moy, min, max );
}

void insertion(ListeC *liste, Couple nv)
{
    /* Création du nouvel élément */
    CelluleC *nouveau = malloc(sizeof(*nouveau));
    if (liste == NULL || nouveau == NULL)
    {
        exit(EXIT_FAILURE);
    }
    nouveau->cou.ppm = nv.ppm;
    nouveau->cou.date.jour = nv.date.jour;
    nouveau->cou.date.mois = nv.date.mois;
    nouveau->cou.date.an = nv.date.an;
    nouveau->cou.heure.h = nv.heure.h;
    nouveau->cou.heure.m = nv.heure.m;
    nouveau->cou.heure.s = nv.heure.s;
    nouveau->cou.lieu.lati = nv.lieu.lati;
    nouveau->cou.lieu.longi = nv.lieu.longi;


    /* Insertion de l'élémentment au début de la liste */
    nouveau->suivant = liste->premier;
    liste->premier = nouveau;
}

void suppression(ListeC *liste)
{
    if (liste == NULL)
    {
        exit(EXIT_FAILURE);
    }

    while (liste->premier != NULL)
    {
        CelluleC *aSupprimer = liste->premier;
        liste->premier = liste->premier->suivant;
        free(aSupprimer);
    }


}

void suppressionM(ListeM *liste)
{
    if (liste == NULL)
    {
        exit(EXIT_FAILURE);
    }

    while (liste->premier != NULL)
    {
        CelluleM *aSupprimer = liste->premier;
        liste->premier = liste->premier->suivant;
        free(aSupprimer);
    }

}

void chargementListeC(ListeC *Ozone, ListeC *Part, ListeC *Carb, ListeC *Sulf, ListeC *Nit, int a, int b) {


    Couple nv; // Struct permettant d'enregistrer les différentes informations, pour les insérer dans les listes
    PPM parent; // Struct permettant d'enregistrer le nombre de PPM par pollutions
    char inspi[TAILLE_MAX] = ""; // Tableau permettant de retenir une ligne de caractère
    char *casser; // Pointeur qui pointe sur les différentes morceau de inspi
    const char *separateurs = " ,-!:"; // Constante de pointeur sur les différents séparateurs


    DIR *rep = NULL;
    struct dirent* fichierLu = NULL; /* Déclaration d'un pointeur vers la structure dirent. */
    rep = opendir("pollution"); /* Ouverture d'un dossier */
    if (rep != NULL) {

    int n = 0;
        while (n != a) // on lit a fichier du répertoire
        { if (b < n) {
                fichierLu = readdir(rep);
                if (strcmp(fichierLu->d_name, ".") != 0 && (strcmp(fichierLu->d_name, "..") != 0)) {
                    char fichier[128] = "";
                    char dossier[128] = "pollution/";
                    snprintf(fichier, sizeof(fichier), fichierLu->d_name);
                    strcat(dossier, fichier); // On concatène fichier dans dossier

                    FILE *fp;
                    fp = fopen(dossier, "r"); //ouverture en lecture
                    if (fp != NULL) {
                        //fichier ouvert avec succes !

                        fgets(inspi, TAILLE_MAX, fp);
                        while (fgets(inspi, TAILLE_MAX, fp) != NULL) {
                            // on casse la chaine inspi en fonction des différents séparateurs et on met les différents morceaux dans les struct Parent et NV
                            casser = strtok(inspi, separateurs);
                            parent.O = atoi(casser);
                            casser = strtok(NULL, separateurs);
                            parent.P = atoi(casser);
                            casser = strtok(NULL, separateurs);
                            parent.C = atoi(casser);
                            casser = strtok(NULL, separateurs);
                            parent.S = atoi(casser);
                            casser = strtok(NULL, separateurs);
                            parent.N = atoi(casser);
                            casser = strtok(NULL, separateurs);
                            nv.lieu.longi = atof(casser);
                            casser = strtok(NULL, separateurs);
                            nv.lieu.lati = atof(casser);
                            casser = strtok(NULL, separateurs);
                            nv.date.an = atoi(casser);
                            casser = strtok(NULL, separateurs);
                            nv.date.mois = atoi(casser);
                            casser = strtok(NULL, separateurs);
                            nv.date.jour = atoi(casser);
                            casser = strtok(NULL, separateurs);
                            nv.heure.h = atoi(casser);
                            casser = strtok(NULL, separateurs);
                            nv.heure.m = atoi(casser);
                            casser = strtok(NULL, separateurs);
                            nv.heure.s = atoi(casser);

                            //On met le bon nombre de PPM dans NV en fonction de la liste dans laquelle on va l'inserer
                            //Puis on insert NV dans la Liste
                            nv.ppm = parent.O;
                            insertion(Ozone, nv);
                            nv.ppm = parent.P;
                            insertion(Part, nv);
                            nv.ppm = parent.C;
                            insertion(Carb, nv);
                            nv.ppm = parent.S;
                            insertion(Sulf, nv);
                            nv.ppm = parent.N;
                            insertion(Nit, nv);

                        }
                        fclose(fp);//fermeture

                    } else
                        printf("Fichier non ouvert \n"); // Fichier non ouvert
                }
                n++;

            }
            else
                n++;
        }

            closedir(rep); //fermeture

    } else
        printf("Répertoire non ouvert \n");// Répertoire non ouvert

}

void global(ListeM *liste, ListeC *Pa) //fonction pour mettre dans la liste des nombre à calculer tout les ppm d'une liste
{
    if ((liste == NULL) && (Pa == NULL ))
    {
        exit(EXIT_FAILURE);
    }

    CelluleC *actuel = Pa->premier;


    while (actuel->cou.ppm != -1)
    {
        insertionC(liste, actuel->cou.ppm);
        actuel = actuel->suivant;
    }

}

void mois(ListeM *liste, ListeC *Pa, Couple nv) //fonction pour mettre par rapport au mois
{
    if ((liste == NULL) && (Pa == NULL ))
    {
        exit(EXIT_FAILURE);
    }

    CelluleC *actuel = Pa->premier;

    while (actuel->cou.ppm != -1)
    {
        if ((actuel->cou.date.mois == nv.date.mois) && (actuel->cou.date.an == nv.date.an))
            insertionC(liste, actuel->cou.ppm);
        actuel = actuel->suivant;
    }
}

void jour(ListeM *liste, ListeC *Pa, Couple nv) //fonction pour mettre par rapport au jour
{
    if ((liste == NULL) && (Pa == NULL ))
    {
        exit(EXIT_FAILURE);
    }

    CelluleC *actuel = Pa->premier;

    while (actuel->cou.ppm != -1)
    {
        if ((actuel->cou.date.mois == nv.date.mois) && (actuel->cou.date.an == nv.date.an) && (actuel->cou.date.jour == nv.date.jour))
            insertionC(liste, actuel->cou.ppm);
        actuel = actuel->suivant;
    }
}

void moment(ListeM *liste, ListeC *Pa, Couple nv) //fonction pour mettre par rapport à un moment précis
{
    if ((liste == NULL) && (Pa == NULL ))
    {
        exit(EXIT_FAILURE);
    }

    CelluleC *actuel = Pa->premier;

    while (actuel->cou.ppm != -1)
    {
        if ((actuel->cou.date.mois == nv.date.mois) && (actuel->cou.date.an == nv.date.an) && (actuel->cou.date.jour == nv.date.jour) && (actuel->cou.heure.s == nv.heure.s) && (actuel->cou.heure.h == nv.heure.h) && (actuel ->cou.heure.m == nv.heure.m))
            insertionC(liste, actuel->cou.ppm);
        actuel = actuel->suivant;
    }
}

int traduction (char *argv[], int argc, int a) // traduit la requête
{

    char o[]="ozone";
    char p[]="particullate_matter";
    char c[]="carbon_monoxide";
    char s[]="sulfure_dioxide";
    char n[]="nitrogen_dioxide";
    char g[]="global";
    char m[]="monthly";
    char d[]="daily";
    char l[]="log";

    if (argc <= 4)
    {
        if (a == 1) {

            if (strcmp(o, argv[2]) == 0 )
                return 1;
            else if (strcmp(p, argv[3]) == 0 )
                return 2;
            else if (strcmp(c, argv[3]) == 0 )
                return 3;
            else if (strcmp(s, argv[3]) == 0 )
                return 4;
            else if (strcmp(n, argv[3]) == 0)
                return 5;
        } else {

            if (strcmp(g, argv[1]) == 0)
                return 1;
            else if (strcmp(m, argv[1]) == 0)
                return 2;
            else if (strcmp(d, argv[1]) == 0)
                return 3;
            else if (strcmp(l, argv[1]) == 0)
                return 4;
        }
    }

            exit(EXIT_FAILURE);
}

void requete(ListeM *liste, ListeC *Ozone, ListeC *Part, ListeC *Carb, ListeC *Sulf, ListeC *Nit, int a, int b, char *argv[])
{
    printf("Traitement en cours ...\n");
    Couple nv;
    char *casser; // Pointeur qui pointe sur les différentes morceau de inspi
    const char *separateurs = " ,-!:"; // Constante de pointeur sur les différents séparateurs


    if (a==1) // Si a=1 on calcule le global des ppm d'une liste
    {
        if (b==1)
        {
            global(liste, Ozone);
        }
        else if (b==2)
        {
            global(liste, Part);
        }
        else if (b==3)
        {
            global(liste, Carb);
        }
        else if (b==4)
        {
            global(liste, Sulf);
        }
        else if (b==5)
        {
            global(liste, Nit);
        }
    } else if (a == 2)
    {

        casser = strtok(argv[2], separateurs);
        nv.date.an = atoi(casser);
        casser = strtok(NULL, separateurs);
        nv.date.mois = atoi(casser);


        if (b==1)
        {

            mois(liste, Ozone, nv);
        }
        if (b==2)
        {
            mois(liste, Part, nv);
        }
        if (b==3)
        {
            mois(liste, Carb, nv);
        }
        if (b==4)
        {
            mois(liste, Sulf, nv);
        }
        if (b==5)
        {
            mois(liste, Nit, nv);
        }

    } else if (a == 3)
    {
        casser = strtok(argv[2], separateurs);
        nv.date.an = atoi(casser);
        casser = strtok(NULL, separateurs);
        nv.date.mois = atoi(casser);
        casser = strtok(NULL, separateurs);
        nv.date.jour = atoi(casser);

        if (b==1)
        {
            jour(liste, Ozone, nv);
        }
        if (b==2)
        {
            jour(liste, Part, nv);
        }
        if (b==3)
        {
            jour(liste, Carb, nv);
        }
        if (b==4)
        {
            jour(liste, Sulf, nv);
        }
        if (b==5)
        {
            jour(liste, Nit, nv);
        }

    } else if (a == 4)
    {
        casser = strtok(argv[2], separateurs);
        nv.date.an = atoi(casser);
        casser = strtok(NULL, separateurs);
        nv.date.mois = atoi(casser);
        casser = strtok(NULL, separateurs);
        nv.date.jour = atoi(casser);
        casser = strtok(argv [3], separateurs);
        nv.heure.h = atoi(casser);
        casser = strtok(NULL, separateurs);
        nv.heure.m = atoi(casser);
        casser = strtok(NULL, separateurs);
        nv.heure.s = atoi(casser);

        if (b==1)
        {
            moment(liste, Ozone, nv);
        }
        if (b==2)
        {
            moment(liste, Part, nv);
        }
        if (b==3)
        {
            moment(liste, Carb, nv);
        }
        if (b==4)
        {
            moment(liste, Sulf, nv);
        }
        if (b==5)
        {
            moment(liste, Nit, nv);
        }
    }
    printf("Traitement terminé \n");

}

int main(int argc, char*argv[]) {

    ListeC *Ozone = initialisationC();
    ListeC *Part = initialisationC();
    ListeC *Carb = initialisationC();
    ListeC *Sulf = initialisationC();
    ListeC *Nit = initialisationC();

    ListeM *Moy = initialisation();

    printf("\nChargement des fichiers en cours ...\n");
    chargementListeC(Ozone, Part, Carb, Sulf, Nit, 350,0);
    chargementListeC(Ozone, Part, Carb, Sulf, Nit, 449, 350);
    printf("Chargement terminer\n");

    requete(Moy, Ozone, Part, Carb, Sulf, Nit, traduction(argv, argc, 0), traduction(argv,argc,1), argv);
    moyenne(Moy);
    printf("Effacement de la mémoire\n");
    suppressionM(Moy);
    suppression(Ozone);
    suppression(Part);
    suppression(Carb);
    suppression(Sulf);
    suppression(Nit);

    return 0;
}
